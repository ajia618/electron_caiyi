// All of the Node.js APIs are available in the preload process.
// It has the same sandbox as a Chrome extension.
let elementConsole, dataSource, dataResult, resultReadArr, successDataLength = 0,
  start_time
window.addEventListener('DOMContentLoaded', () => {
  elementConsole = document.getElementById('draw')
})
document.addEventListener('drop', (e) => {
  e.preventDefault();
  e.stopPropagation();
  for (const f of e.dataTransfer.files) {
    readFile(f.path)
    start_time = new Date().getTime()
    rendHtml('===================================================')
    rendHtml('时间：' + new Date())
    rendHtml(`文件名: ${f.name}`)
    rendHtml(`文件大小: ${(f.size / 1024).toFixed(4)}kb`)
    rendHtml('读取文件...')
  }
});
document.addEventListener('dragover', (e) => {
  e.preventDefault();
  e.stopPropagation();
});


function readFile(path) {
  let fs = require("fs");
  fs.readFile(path, function (err, data) {
    if (err) return console.error(err);
    rendHtml('开始分析...')
    let source = data.toString().split('\n')
    let result = source.splice(0, 1)[0]
    dataResult = trimAdd(result)
    resultReadArr = dataResult.red.split(' ')
    dataSource = source.map((item) => {
      return formatTxt(item)
    })
    exportResult()
  });
}

// text文件格式化
function formatTxt(data) {
  data.trim(data)
  let data1 = data.split('=>')
  let data_before = trimAdd(data1[0]).red
  let fullLength = getRedC(data_before, 6)
  let blue = trim(trim(data1[0]).split('+')[1])
  let maxRedNum = 0
  let resultMaxRedNum = getHeightRedNum(data_before).intersection.length
  let successLength = 0
  let successData = [],
    fallData = []
  let data_after = trim(data1[1]).split('|').map((item) => {
    let red = trimAdd(item).red
    // 分析结果和开奖结果对比数据
    let redAnalyse = getHeightRedNum(red)
    // 获取分析结果最高单注中奖红球数
    let intersectionLength = redAnalyse.intersection.length
    maxRedNum = intersectionLength > maxRedNum ? intersectionLength : maxRedNum
    if (maxRedNum > 0 && (intersectionLength >= resultMaxRedNum)) {
      successData.push(red + '|' + redAnalyse.intersection.join(' '))
      successLength++
    } else {
      fallData.push(red)
    }
    return red
  })
  let analyseLength = data_after.length
  let failLength = analyseLength - successLength
  let successPercentage = (successLength / analyseLength * 100).toFixed(4)
  if (successLength > 0) {
    successDataLength++
  }
  return {
    blue,
    data_before,
    data_after,
    fullLength,
    analyseLength,
    maxRedNum,
    resultMaxRedNum,
    successLength,
    successData,
    failLength,
    fallData,
    successPercentage
  }
}

//去左右空格;
function trim(s) {
  return s.replace(/(^\s*)|(\s*$)/g, "");
}

// 去“+”号
function trimAdd(s) {
  let arr = s.split('+')
  return {
    red: trim(arr[0]),
    blue: trim(arr[1])
  }
}

// 对比分析值
function getHeightRedNum(res) {
  let arr1 = res.split(' ')
  let arr2 = resultReadArr
  // 交集
  let intersection = arr1.filter(function (val) {
    return arr2.indexOf(val) > -1
  })
  // 并集
  let union = arr1.concat(arr2.filter(function (val) {
    return !(arr1.indexOf(val) > -1)
  }))
  // 补集 两个数组各自没有的集合
  let complement = arr1.filter(function (val) {
      return !(arr2.indexOf(val) > -1)
    })
    .concat(arr2.filter(function (val) {
      return !(arr1.indexOf(val) > -1)
    }))
  // 差集 数组arr1相对于arr2所没有的
  let diff = arr1.filter(function (val) {
    return arr2.indexOf(val) === -1
  })
  return {
    intersection,
    union,
    complement,
    diff
  }
}

// 获取全排列数
function getRedC(s, m) {
  let arr = s.split(' ')
  return combination(arr.length, m)
}

// 全排列
function combination(n, m) {
  let a, b, c;
  a = b = c = 1;
  for (; c <= m;) {
    b *= c++;
    a *= n--;
  }
  let count = Math.abs(a / b);
  return count;
}

// 输出总结果
function exportResult() {
  let total = dataSource.length
  let fullNotes = 0
  let analyseNotes = 0
  dataSource.map((item) => {
    fullNotes += item.fullLength
    analyseNotes += item.analyseLength
  })
  let successPercentage = successDataLength / total * 100
  let end_time = new Date().getTime()
  rendHtml(`分析人次：${dataSource.length}`)
  rendHtml(`分析前注数：${fullNotes}`)
  rendHtml(`分析后注数：${analyseNotes}`)
  rendHtml(`正确人次：${successDataLength}`)
  rendHtml(`正确率：${successPercentage}%`)
  rendHtml(`用时：${(end_time-start_time) / 1000}S`)
}

function rendHtml(text) {
  let div = document.createElement('div')
  div.innerText = text
  if (elementConsole) elementConsole.appendChild(div)
  elementConsole.scrollTop = elementConsole.scrollHeight
}