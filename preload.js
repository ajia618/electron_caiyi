const { app } = require('electron').remote
let fs = require("fs");
let readline = require("readline");
let times = 0
let fileSize = 0
let elementConsole, // 展示结果Dom
  elementRadio,
  elementSuccess,
  elementError,
  elementQuitApp,
  errorResultArr,
  successResultArr,
  type = 6,
  dataSource=[], // 数据源
  dataResult, // 开奖数据
  resultReadArr, // 开奖红球数组
  successDataLength = 0, // 成功分析数
  start_time; // 开始时间
window.addEventListener('DOMContentLoaded', () => {
  elementConsole = document.getElementById('draw')
  elementRadio = document.getElementById('typeName')
  elementSuccess = document.getElementById('successResult')
  elementError = document.getElementById('errorResult')
  elementQuitApp = document.getElementById('quitApp')
  elementQuitApp.addEventListener('click', function () {
    app.quit()
  })
})

document.addEventListener('drop', (e) => {
  e.preventDefault();
  e.stopPropagation();
  for (const f of e.dataTransfer.files) {
    times++
    start_time = new Date().getTime()
    rendHtml('===================================================')
    rendHtml('时间：' + new Date())
    rendHtml(`文件名: ${f.name}`)
    fileSize = f.size
    if (fileSize < 1024) {
      rendHtml(`文件大小: ${f.size}b`)
    } else if (fileSize >= 1024 && fileSize < 1048576) {
      rendHtml(`文件大小: ${parseInt(f.size / 1024 * 10000) / 10000}kb`)
    } else {
      rendHtml(`文件大小: ${parseInt(f.size / 1048576 * 10000) / 10000}mb`)
    }
    rendHtml('读取文件...')
    type = elementRadio.value || 6
    let i = 0;
    let div = document.createElement('div')
    div.id = 'numBox' + times
    div.innerText = i
    if (elementConsole) elementConsole.appendChild(div)
    elementConsole.scrollTop = elementConsole.scrollHeight
    let numBox = document.getElementById('numBox' + times)
    readliner = readline.createInterface({
      input: fs.createReadStream(f.path),
    });
    let persent = 0
    let size = 0
    dataSource = []
    successDataLength = 0
    errorResultArr = []
    successResultArr = []

    readliner.on('line', function (chunk) {
      // console.log('chunk', chunk)
      //处理每一行数据
      size += chunk.length
      if (i === 0) {
          dataResult = trimAdd(chunk)
          resultReadArr = dataResult.red.split(' ')
      } else {
          dataSource.push(formatTxt(chunk));
      }
      i++;
      persent = parseInt(size / fileSize * 100)
      if (persent > 100) {
        persent = 100
      }
      numBox.innerText = persent + '%'
    });

    readliner.on('close', function () {
        numBox.innerText = '100%'
        exportResult()
    });
  }
});
document.addEventListener('dragover', (e) => {
  e.preventDefault();
  e.stopPropagation();
});

// text文件格式化
function formatTxt(data) {
    data.trim(data)
    let data1 = data.split('=>')
    let data_before = trimAdd(data1[0]).red
    let fullLength = getRedC(data_before, type)
    let blue = trim(trim(data1[0]).split('+')[1])
    let resultMaxRedNum = getHeightRedNum(data_before).intersection.length // 原数据红球中奖个数
    // rendHtml(resultMaxRedNum +':'+ getHeightRedNum(data_before).intersection.join(' ')) // 调试-输出中奖红球数据
    let maxRedNum = 0 // 分析数据中奖红球最大个数
    let successLength = 0
    let successData = [],
        fallData = [],
        isSuccess = false
    let data_after = trim(data1[1]).split('|').map((item) => {
        let red = trimAdd(item).red
        // 分析结果和开奖结果对比数据
        let redAnalyse = getHeightRedNum(red)
        // 获取分析结果最高单注中奖红球数
        let intersectionLength = redAnalyse.intersection.length
        maxRedNum = intersectionLength > maxRedNum ? intersectionLength : maxRedNum
        if (intersectionLength >= resultMaxRedNum) {
            successData.push(red + '|' + redAnalyse.intersection.join(' '))
            successLength++
        } else {
            fallData.push(red)
        }
        return red
    })
    if (maxRedNum < resultMaxRedNum) {
      errorResultArr.push(data_before + ' + ' + blue)
      isSuccess = false
    } else {
      successResultArr.push(data_before + ' + ' + blue)
      isSuccess = true
    }
    let analyseLength = data_after.length
    let failLength = analyseLength - successLength
    let successPercentage = parseInt(successLength / analyseLength * 100 * 10000) / 10000
    if (successLength > 0) {
        successDataLength++
    }
    return {
        blue,
        data_before,
        data_after,
        fullLength,
        analyseLength,
        maxRedNum,
        resultMaxRedNum,
        successLength,
        successData,
        failLength,
        fallData,
        successPercentage,
        isSuccess
    }
}

//去左右空格;
function trim(s) {
    return s.replace(/(^\s*)|(\s*$)/g, "");
}

// 去“+”号
function trimAdd(s) {
    let arr = s.split('+')
    return {
        red: trim(arr[0]),
        blue: trim(arr[1])
    }
}

// 对比分析值
function getHeightRedNum(res) {
    let arr1 = res.split(' ')
    let arr2 = resultReadArr
    // 交集
    let intersection = arr1.filter(function (val) {
        return arr2.indexOf(val) > -1
    })
    // 并集
    let union = arr1.concat(arr2.filter(function (val) {
        return !(arr1.indexOf(val) > -1)
    }))
    // 补集 两个数组各自没有的集合
    let complement = arr1.filter(function (val) {
            return !(arr2.indexOf(val) > -1)
        })
        .concat(arr2.filter(function (val) {
            return !(arr1.indexOf(val) > -1)
        }))
    // 差集 数组arr1相对于arr2所没有的
    let diff = arr1.filter(function (val) {
        return arr2.indexOf(val) === -1
    })
    return {
        intersection,
        union,
        complement,
        diff
    }
}

// 获取全排列数
function getRedC(s, m) {
    let arr = s.split(' ')
    return combination(arr.length, m)
}

// 全排列
function combination(n, m) {
    let a, b, c;
    a = b = c = 1;
    for (; c <= m;) {
        b *= c++;
        a *= n--;
    }
    let count = Math.abs(a / b);
    return count;
}

// 输出总结果
function exportResult() {
  let total = dataSource.length
  let fullNotes = 0
  let analyseNotes = 0
  dataSource.map((item) => {
    fullNotes += item.fullLength
    analyseNotes += item.analyseLength
  })
  let successPercentage = parseInt(successDataLength / total * 100 * 10000) / 10000
  let end_time = new Date().getTime()
  rendHtml(`分析人次：${total}`)
  rendHtml(`分析前注数：${fullNotes}`)
  rendHtml(`分析后注数：${analyseNotes}`)
  rendHtml(`正确人次：${successDataLength}`)
  rendHtml(`正确率：${successPercentage}%`)
  rendHtml(`用时：${(end_time-start_time) / 1000}S`)
  if (elementSuccess.checked || elementError.checked) {
    rendHtml('结果输出中...')
  }
  setTimeout(() => {
    showSuccessResult()
    showErrorResult()
    rendHtml('===================================================')
  }, 500);
}

function rendHtml(text) {
  let div = document.createElement('div')
  div.innerHTML = text
  if (elementConsole) elementConsole.appendChild(div)
  elementConsole.scrollTop = elementConsole.scrollHeight
}

// 显示成功结果
function showSuccessResult () {
  if (elementSuccess.checked) {
    let arr = successResultArr
    rendHtml('成功结果：')
    rendHtml('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
    if (arr.length) {
      for (let i = 0; i < arr.length; i++) {
          rendHtml(arr[i])
      }
    } else {
      rendHtml('无数据')
    }
    rendHtml('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
  }
}
// 显示失败结果
function showErrorResult () {
  if (elementError.checked) {
    let arr = errorResultArr
    rendHtml('失败结果：')
    rendHtml('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
    if (arr.length) {
      for (let i = 0; i < arr.length; i++) {
          rendHtml(arr[i])
      }
    } else {
      rendHtml('无数据')
    }
    rendHtml('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
  }
}
